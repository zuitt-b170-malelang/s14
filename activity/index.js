/* Activity:
1. In the S14 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Create multiple variables that will store the different JavaScript data types containing information relating to user details
4. Create a function named printUserInfo that will accept the following information:
- First name
- Last name
- Age
5. The printUserInfo function will print those details in the console as a single string. 
This function will also print the hobbies and work address.
6. Create another function named returnFunction that will return a value and store it in a variable.
8. Create a git repository named S14.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle. */


let fname = "John";
let lname = "Smith";
let age = 30
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {houseNumber: "32", street: "Washington", city: "Lincoln", state: "Nebraska"};

console.log(`First Name: ${fname}`);
console.log(`Last Name: ${lname}`);
console.log(`Age ${age}`);
console.log("Hobbies:");
console.log(hobbies)
console.log("Workadress:")
console.log(workAddress)



function printUserInfo(firstName, lastName, age){
 console.log(`${firstName} ${lastName} is ${age} years of age`)
 console.log("This was printed inside of the function")
 console.log(hobbies)
 console.log("This was printed inside of the function")
 console.log(workAddress)
 
}

printUserInfo("John", "Smith", 30);

let isMarried=true;

function returnFunction(){
  return `the value of isMarried is ${isMarried}`
}
let returnFunctionNow= returnFunction(isMarried);
console.log(returnFunctionNow);