//one line comment (ctrl + slash)
/*
  multiline comment (alt + shift + a)
*/

console.log("Hello Batch 170!");


/* 
  Javascript - we can see the messages/log in the console

  Browser Consoles are part of the browsers which will allow us to see/log the messages, data or information from the programming language. They are easily accessed in the dev tools

  Statements - instructions, expressions that we add to the programming language to communicate with the computers.
  Usually ending with a semicolon(;) However, JS has implemented a way to automatically add semicolons at the end of the line.

  Syntax - set of rules that describes how statements should be made/constructed
  Lines/blocks of codes must follow a certain set of rules for it to work properly
*/

console.log("EJ")
/* 
  create 3 console logs to display your favorite food
*/
/* console.log("buko pandan")
console.log("beef caldereta")
console.log("any Pinoy streetfood") */

let food1 = "adobo"
console.log(food1)

/* 
  Variables are way to store information or data within the JS

  to create a variable, we first declare the name of the variable with either "let/const" keyword:
      let <name of variable>

    Then, we can initialize(define) the variable with a value or data
      let <name of var> = <value>
*/

console.log("My favorite food is: " + food1);

let food2 = "Kare Kare";
console.log("My favorite food is: " + food1 + " and " + food2);

let food3;


/* 
  we can create variable without values

  then, var=undefined
*/

console.log(food3);
/* 
  we can update the content of a variable by reassigning the value using an assignment operator (=);

  assignment operator (=) let us (re)assign values to a variable
*/
food3="ChickenJoy";
console.log(food3)

/* 
we cannot set another variable with the same name. it will result into an error

let food1="flat tops"
console.log(food1) */

// const keyword
const pi = 3.1416;
console.log(pi);
/* 
  const - allow creation of variable (constant   variable)
        - cannot be reassigned another value
        - cannot creat a const variable without assigning a value
*/

/* pi="pizza";

console.log(pi); */

// const gravity;

// console.log(gravity);

/* 
Miniactivity

LET keyword:
-reassign a new value for food1 with another favorite food of yours
-reassign a new value for food2 with another favorite food of yours
-log the values of both variables in the console

CONST keyword
-create a const variable called sunriseDirection with East as its value
-create a const variable called sunsetDirection with West as its value
*/

food1="chocolate";
food2="steak";

const sunriseDirection = "East";
const sunsetDirection="West";

console.log(food1 + " & " + food2)

console.log(sunriseDirection);
console.log(sunsetDirection);

/* 
  Guidlines in creating JS variable

  1. We can create a let variable with the let keyword. let variables can be reassigned but not be redeclared.

  2. Creating a variable has two parts: Declaration of the variable and initialization of the initial value of the variable through the use of assignment operator (=).

  3. A let variable can be declared without initialization. However, the value of the variable will be undefined until it os reassigned with a value.

  4. Not defined vs Undefined. Not defined error means that the variable is used but not declared. Undefined results from a variable that is used but not initialized.
  
  5. We can use const keyword to create variables. Constant variables cannot be declared without initialization. Constant variables cannot be reassigned.

  6. When creating variable names, start with small caps, this is because we are avoiding conflict with JS that is named with capital letters

  7. If the variable would need two words, the naming convention is the use of camelCase. Do not add any space/s for the variables with words as names.




*/

/* 
// data types

*/
// string
/* strings are data type which are alphanumerical text. They could be a name, phrase, or even sentence. We can create them with as single quote(') or double quote(") 

The text inside the quotation marks will be displayed as it is.

variable = "<string>";
*/
console.log("Sample String Data");

// Numeric Data Types
/* 
  data types which are numeric,. numeric data types are displayed when numbers are not placed in the quotation marks

  If there is mathematical operation needed to be done, numeric data type should be used instead of string
*/

console.log(0123456789);
console.log(1+1);

/* 
  miniactivity
  using string data type
  -display in the console your name(full name)
  -birthday
  -province where you live

  using numeric data type
  display in the console your:
    high score in the last game you played
    your favorite number
    your favorite number in electric fan/aircon
*/

console.log("Ethan Malelang");
console.log("August 5, 1995");
console.log("Rizal");
console.log(99);
console.log(5);
console.log(2);

