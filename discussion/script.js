// JS Function
/* 
  Functions are used to create reuseable commans/statements that prevents the dev from typing a bunch of codes.
  In field, a big number of lines of codes is the normal output; using functions would save the dev a lot of time and effort in typing the codes that will be used multiple times.

  SYNTAX:
    function <functionName> (parameters){
      <command/statement>
    };
*/
function printStar(){
  console.log("*")
};

printStar();
printStar();
printStar();
printStar();

/* 
  functions can also use aprameters. these parameters can be defined and a part of the command inside the function. When called, parameters can be replaced with the target value op the developer.
  Make sure that the value is inside quotation marks when called
*/
function sayHello(name){
  console.log("Hello " + name)
};

sayHello("EJ");



/*function alertPrint(){
	alert("Hello");
	console.log("Hello")
};

alertPrint(); */

function add(x, y){
  let sum = x+y
  console.log(sum)
};
add (1,2);

// three parameters

function printBio(fname, lname, age){
// console.log("Hello " + fname + " " + lname + " " + age)

// using template literals
/* 
  declared by using backticks with dollar sign and curly braces with the parameters inside the braces.
*/
// backticks(`) - 
console.log(`Hello ${fname} ${lname} ${age}`)
};

printBio("Ethan", "Malelang", 27);

function createFullName(fname, mname, lname){
  /* 
  return specifies the value to be given back by the function once it is finishing executing. the value can be  given to a variable.
  It only gives value, but does not display them in the console. That's why we also need to console.log
  */
  return `${fname} ${mname} ${lname}`
}

let fullName = createFullName("Juan", "Dela", "Cruz");
console.log(fullName);